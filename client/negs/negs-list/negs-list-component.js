angular.module('better-thoughts').directive('negsList', function() {
  return {
    restrict: 'E',
    templateUrl: 'client/negs/negs-list/negs-list.html',
    controllerAs: 'negsList',
    controller: function ( $scope, $reactive, $modal ) {
      $reactive( this ).attach( $scope );

      this.newNeg = {};
      this.perPage = 3;
      this.page = 1;
      this.sort = {
        name: 1
      };
      this.orderProperty = '1';
      this.searchText = '';

      this.subscribe('negs', () => {
        return [
          {
            limit: parseInt(this.perPage),
            skip: parseInt((this.getReactively('page') - 1) * this.perPage),
            sort: this.getReactively('sort')
          },
          this.getReactively('searchText')
        ]
      });

      this.helpers({
        negs: () => {
          return Negs.find({}, { sort : this.getReactively('sort') });
        },
        negsCount: () => {
          return Counts.get('numberOfNegs');
        },
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUserId: () => {
          return Meteor.userId();
        }
      });

      this.addNeg = () => {
        this.newNeg.owner = Meteor.user()._id;
        Negs.insert(this.newNeg);
        this.newNeg = {};
      };

      this.removeNeg = (neg) => {
        Negs.remove({_id: neg._id});
      };

      this.pageChanged = (newPage) => {
        this.page = newPage;
      };


      this.updateSort = () => {
        this.sort = {
          name: parseInt(this.orderProperty)
        }
      };


      this.openAddNewNegModal = function () {
        $modal.open({
          animation: true,
          template: '<add-new-neg-modal></add-new-neg-modal>'
        });
      };

    }
  }
});