angular.module('better-thoughts').directive('betterThoughts', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/negs/better-thoughts/better-thoughts.html',
    controllerAs: 'betterThoughts',
    controller: function ($scope, $state, $stateParams, $reactive, $modal) {
      $reactive(this).attach($scope);

      this.newBetterThought = {};

      this.subscribe('negs');

      this.helpers({
        neg: () => {
          return Negs.findOne({ _id: $stateParams.negId });
        },
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        },
        currentUserId: () => {
          return Meteor.userId();
        }
      });

      this.save = () => {
        Negs.update({_id: $stateParams.negId}, {
          $set: {
            name: this.neg.name,
          }
        }, (error) => {
          if (error) {
            console.log('Oops, unable to update the thought...');
          }
          else {
            console.log('Done!', $stateParams);
          }
        });
      };

      this.addBetterThought = () => {
        Negs.update(
          { _id : $stateParams.negId },
          {
            $push:
              { betterThoughts: {
                name : this.newBetterThought.name,
                _id : Random.id()
              }
            }
          }
        );
        this.newBetterThought = {};
      };

      this.removeBetterThought = (betterThought) => {
        Negs.update(
          { _id : $stateParams.negId },
          {
            $pull: {
              betterThoughts: {
                _id: betterThought._id
              }
            }
          }
        );
      };

      this.openAddNewBtModal = function () {
        $modal.open({
          animation: true,
          template: '<add-new-bt-modal></add-new-bt-modal>'
        });
      };

    }
  };
});