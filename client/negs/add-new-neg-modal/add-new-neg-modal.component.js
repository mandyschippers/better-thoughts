angular.module('better-thoughts').directive('addNewNegModal', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/negs/add-new-neg-modal/add-new-neg-modal.html',
    controllerAs: 'addNewNegModal',
    controller: function ($scope, $stateParams, $reactive) {
      $reactive(this).attach($scope);

      this.helpers({
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        }
      });

      this.newNeg = {};

      this.addNewNeg = () => {
        this.newNeg.owner = Meteor.userId();
        Negs.insert(this.newNeg);
        this.newNeg = {};
        $scope.$close();
      };
    }
  }
});