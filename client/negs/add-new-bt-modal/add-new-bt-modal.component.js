angular.module('better-thoughts').directive('addNewBtModal', function () {
  return {
    restrict: 'E',
    templateUrl: 'client/negs/add-new-bt-modal/add-new-bt-modal.html',
    controllerAs: 'addNewBtModal',
    controller: function ($scope, $stateParams, $reactive) {
      $reactive(this).attach($scope);

      this.helpers({
        isLoggedIn: () => {
          return Meteor.userId() !== null;
        }
      });

      this.newBetterThought = {};

      this.addBetterThought = () => {
        Negs.update(
          { _id : $stateParams.negId },
          {
            $push:
              { betterThoughts: {
                name : this.newBetterThought.name,
                _id : Random.id()
              }
            }
          }
        );
        this.newBetterThought = {};
        $scope.$close();
      };


    }
  }
});