angular.module('better-thoughts').config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
  $locationProvider.html5Mode(true);

  $stateProvider
    .state('thoughts', {
      url: '/thoughts',
      template: '<negs-list></negs-list>'
    })
    .state('betterThoughts', {
      url: '/better-thoughts/:negId',
      template: '<better-thoughts></better-thoughts>'
    })
    .state('betterThoughtDetails', {
      url: '/better-thought-details/:betterThoughtId',
      template: '<better-thought-details></better-thought-details>'
    });
  $urlRouterProvider.otherwise("/thoughts");


});