Negs = new Mongo.Collection("negs");

if (Meteor.isClient) {
  angular.module('better-thoughts', [
    'angular-meteor',
    'ui.router'
    ]);


  angular.module('better-thoughts').config(function ($urlRouterProvider, $stateProvider, $locationProvider) {
    $locationProvider.html5Mode(true);

    $stateProvider
      .state('thoughts', {
        url: '/thoughts',
        template: '<negs-list></negs-list>'
      })
      .state('betterThoughts', {
        url: '/thoughts/:negId',
        template: '<better-thoughts></better-thoughts>'
      });

    $urlRouterProvider.otherwise("/thoughts");
  });

  angular.module('better-thoughts').directive('negsList', function() {
    return {
      restrict: 'E',
      templateUrl: 'negs-list.html',
      controllerAs: 'negsList',
      controller: function ( $scope, $reactive ) {
        $reactive( this ).attach( $scope );

        this.newNeg = {};

        this.helpers({
          negs: () => {
            return Negs.find({});
          }
        });

        this.addNeg = () => {
          Negs.insert(this.newNeg);
          this.newNeg = {};
        };

        this.removeNeg = (neg) => {
          Negs.remove({_id: neg._id});
        }
      }
    }
  });


  angular.module('better-thoughts').directive('betterThoughts', function () {
    return {
      restrict: 'E',
      templateUrl: 'better-thoughts.html',
      controllerAs: 'betterThoughts',
      controller: function ($scope, $stateParams, $reactive) {
        $reactive(this).attach($scope);

        this.helpers({
          neg: () => {
            return Negs.findOne({ _id: $stateParams.negId });
          }
        });

        this.save = () => {
          Negs.update({_id: $stateParams.negId}, {
            $set: {
              name: this.neg.name,
            }
          }, (error) => {
            if (error) {
              console.log('Oops, unable to update the thought...');
            }
            else {
              console.log('Done!');
            }
          });
        };
      }
    }
  });
}