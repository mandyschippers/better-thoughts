//later rework to insert thoughts when new user is created

Accounts.onCreateUser (function(options, user) {
  if(!options || !user) {
    console.log('error creating user');
    return;
  }

  user._id = Meteor.users._makeNewID();

  var negs = [
        {
          'name': 'Nobody cares',
          'betterThoughts': [
            {
              'name': 'People do care about me',
              '_id' : Random.id()
            },
            {
              'name': 'If I care about others, others must also care about me',
              '_id' : Random.id()
            }
          ],
          'owner' : user._id
        },
        {
          'name': 'If I don’t do well, I am a failure',
          'betterThoughts': [
            {
              'name': 'Failing is the only way to learn',
              '_id' : Random.id()
            },
            {
              'name': 'Try, and try again. A single failure is not important',
              '_id' : Random.id()
            }
          ],
          'owner' : user._id
        },
        {
          'name': 'I can’t do it',
          'betterThoughts': [
            {
              'name': 'I can do whatever I put my mind to',
              '_id' : Random.id()
            },
            {
              'name': 'A thousand mile journey starts with a single step',
              '_id' : Random.id()
            }
          ],
          'owner' : user._id
        }
      ];

    for (var i = 0; i < negs.length; i++) {
      Negs.insert(negs[i]);
    }

  return user;
});