Negs = new Mongo.Collection("negs");

Negs.allow({
  insert: function (userId, neg) {
    return userId && neg.owner === userId;
  },
  update: function (userId, neg, fields, modifier) {
    return userId && neg.owner === userId || userId && neg.owner === 'init' ;
  },
  remove: function (userId, neg) {
    return userId && neg.owner === userId || userId && neg.owner === 'init' ;
  }
});

